import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from 'recompose';

import {SignUpLink} from '../SignUp/';
import {withFirebase} from '../Firebase';
import * as ROUTES from '../../constants/routes';
import './SignIn.css';


const SignInPage = () => (
    <div>
        <h1 className="signIn__title">PICO</h1>
        <SignInForm/>
        <SignUpLink/>
    </div>
);

const INITIAL_STATE = {
    email: '',
    password: '',
    error: null,
};

class SignInFormBase extends Component {
    constructor(props) {
        super(props);
        this.state = INITIAL_STATE;
    }
    onSubmit = event => {
        const {email, password} = this.state;
        this.props.firebase
        .doSignInWithEmailAndPassword(email, password)
        .then(() => {
            this.setState({...INITIAL_STATE});
            this.props.history.push(ROUTES.HOME+ROUTES.SECTION_SOLO);
        })
        .catch(error => {
            this.setState({error});
        });
        event.preventDefault();

    }
    onChange = (event) => {
        const {name, value} = event.target;
        this.setState({[name]: value})
    }
    render() {
        const {email, password, error} = this.state;
        const isInvalid = password === '' || email === '';
        return(
            <form onSubmit={this.onSubmit}>
                <div className="signIn__credential">
                    <input
                        type="text"
                        placeholder="email"
                        name="email"
                        value={email}
                        className="signIn__credential__input first"
                        onChange={this.onChange}
                    />
                    <input
                        type="password"
                        placeholder="password"
                        name="password"
                        value={password}
                        className="signIn__credential__input"
                        onChange={this.onChange}
                    />
                    <button 
                        className="signIn__btn__submit" 
                        disabled={isInvalid} 
                        type="submit">
                        Login
                    </button>
                </div>
                {error && <p>{error.message}</p>}
            </form>
        );
    }
}

const SignInForm = compose(
    withRouter,
    withFirebase,
)(SignInFormBase);

export default SignInPage;

export {SignInForm};