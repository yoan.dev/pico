import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import {withFirebase} from '../Firebase';
import * as ROUTES from '../../constants/routes';
import './SignUp.css';

const SignUpPage = () => (
    <div>
        <h1 className="signUp__title">PICO</h1>
        <SignUpForm/>
    </div>
);

const INITIAL_STATE = {
    username: '',
    email: '',
    passwordOne: '',
    passwordTwo: '',
    error: null,
};

class SignUpFormBase extends Component {
    constructor(props) {
        super(props);
        this.state = INITIAL_STATE;
    }
    onSubmit = event => {
        const {username, email, passwordOne} = this.state;
        this.props.firebase
        .doCreateUserWithEmailAndPassword(email, passwordOne)
        .then(authUser => {
            this.setState({...INITIAL_STATE});
            this.props.history.push(ROUTES.HOME+ROUTES.SECTION_SOLO);
        })
        .catch(error => {
            this.setState({error})
        });
        event.preventDefault();
    }
    onChange = event => {
        this.setState({[event.target.name]: event.target.value});
    }
    render() {
        const {
            username,
            email,
            passwordOne,
            passwordTwo,
            error,
        } = this.state;
        const isInvalid = 
            passwordOne !== passwordTwo ||
            username === '' ||
            email === '';
        return(
            <form onSubmit={this.onSubmit}>
                <div className="signUp__credential">
                    <input 
                    type="text"
                    placeholder="Full name"
                    name="username"
                    value={username}
                    className="signUp__credential__input first"
                    onChange={this.onChange}
                    />
                    <input 
                    type="text"
                    placeholder="Email address"
                    name="email"
                    value={email}
                    className="signUp__credential__input"
                    onChange={this.onChange}
                    />
                    <input
                    type="password"
                    placeholder="Password"
                    name="passwordOne"
                    value={passwordOne}
                    className="signUp__credential__input"
                    onChange={this.onChange}
                    />
                    <input 
                    type="password"
                    placeholder="confirm Password"
                    name="passwordTwo"
                    value={passwordTwo}
                    className="signUp__credential__input"
                    onChange={this.onChange}
                    />
                    <button className="signUp__btn__submit" disabled={isInvalid} type="submit">Sign Up</button>
                </div>
                {error && <p>{error.message}</p>}
            </form>
        );
    }
}

const SignUpLink = () => (
    <p>
        Don't have an account? <Link to={ROUTES.SIGN_UP} className="signUp__link">Sign Up</Link>
    </p>
);

const SignUpForm = compose(
    withRouter,
    withFirebase,
)(SignUpFormBase);

export default SignUpPage;

export {
    SignUpForm,
    SignUpLink,
};