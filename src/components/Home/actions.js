import {NAV_HOME_LOAD} from '../../constants/actionsTypes';

const doLoadNavHome = url => ({
    type: NAV_HOME_LOAD,
    url,
});

export {
    doLoadNavHome,
};