import {
    NAV_HOME_CHANGE,
    NAV_HOME_LOAD,
    } from '../../constants/actionsTypes';

const INITIAL_STATE = {
    homeNav: [1,0,0,0,0],
};

const homeReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case NAV_HOME_CHANGE: {
            return applyChangeHomeNav(state, action);
        }
        case NAV_HOME_LOAD: {
            return applyLoadNavHome(state, action);
        }
        default: return state;
    }
}
const applyChangeHomeNav = (state, action) => {
    const homeNav = [...state.homeNav];
    const prevIndex = homeNav.indexOf(2);
    const prevIdFocus = homeNav.indexOf(1);
    if (prevIndex !== -1) homeNav[prevIndex] = 0;
    [homeNav[prevIdFocus], homeNav[action.id]] = [2, 1];
    return {...state, homeNav};
}
const applyLoadNavHome = (state, action) => {
    let id;
    if (/solo/i.test(action.url)) id=0;
    if (/challenge/i.test(action.url)) id=1;
    if (/duel/i.test(action.url)) id=2;
    if (/achievement/i.test(action.url)) id=3;
    if (/store/i.test(action.url)) id=4;
    const homeNav = [...state.homeNav];
    homeNav[homeNav.indexOf(1)] = 0;
    homeNav[id] = 1;
    return {...state, homeNav};
}


export default homeReducer;