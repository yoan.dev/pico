import React from 'react';
import {connect} from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';

import {doLoadNavHome} from './actions';
import Navigation from '../Navigation';
import SectionScrolling from '../Section/SectionScrolling';
import PlaySolo from '../Play/PlaySolo';
import './Home.css';

const Home = ({onLoadHome}) => {
    const url = window.location.href;
    onLoadHome(url);
    return(
        <div>
            <Router>
                <div className="home">
                    <div className="home__section">
                        <Switch>
                            <Route exact path={ROUTES.HOME+ROUTES.SECTION_SOLO} 
                                    render={(props) => <SectionScrolling {...props}
                                    component={"solo"}
                            />} />
                            <Route exact path={ROUTES.HOME+ROUTES.SECTION_CHALLENGE} 
                                    render={(props) => <SectionScrolling {...props} 
                                    component={"challenge"}
                            />} />
                            <Route exact path={ROUTES.HOME+ROUTES.SECTION_DUEL} 
                                    render={(props) => <SectionScrolling {...props} 
                                    component={'duel'}
                            />} />
                            <Route exact path={ROUTES.HOME+ROUTES.SECTION_ACHIEVEMENT} 
                                    render={(props) => <SectionScrolling {...props} 
                                    component={'achievement'}
                            />} />
                            <Route exact path={ROUTES.HOME+ROUTES.SECTION_STORE} 
                                    render={(props) => <SectionScrolling {...props} 
                                    component={'store'}
                            />} />
                            <Route exact path={ROUTES.PLAY+ROUTES.SOLO} 
                                    render={(props) => <PlaySolo {...props} 
                            />} />
                        </Switch>
                    </div>
                    <Navigation />
                </div>
            </Router>
        </div>
    );}


const mapDispatchToProps = dispatch => ({
    onLoadHome: url => dispatch(doLoadNavHome(url)),
})

export default connect(null, mapDispatchToProps)(Home);

