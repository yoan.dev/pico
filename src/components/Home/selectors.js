const getIsHome = state => state.isHome;

export {
    getIsHome,
};