import React from 'react';

import './DiceBlack1.css';

const DiceBlack1  = ({id}) => 
    <div className="dice__black__1">
        {(id===1 || id===3 || id===5) && <div className="dice__point1__basic"></div>}
        {(id===2 || id===3 || id===4 || id===5 || id===6) && <div className="dice__point2__basic"></div>}
        {id===6  && <div className="dice__point3__basic"></div>}
        {(id===4 || id===5 || id===6) && <div className="dice__point4__basic"></div>}
        {(id===2 || id===3 || id===4 || id===5 || id===6) && <div className="dice__point5__basic"></div>}
        {id===6  && <div className="dice__point6__basic"></div>}
        {(id===4 || id===5 || id===6) && <div className="dice__point7__basic"></div>}
    </div>

export default DiceBlack1;