import React from 'react';

import './DiceWhite1.css';

const DiceWhite1 = ({id}) => {
    return(
        <div className="dice__white__1">
             {(id===1 || id===3 || id===5) && <div className="dice__point1__black"></div>}
             {(id===2 || id===3 || id===4 || id===5 || id===6) && <div className="dice__point2__black"></div>}
             {id===6  && <div className="dice__point3__black"></div>}
             {(id===4 || id===5 || id===6) && <div className="dice__point4__black"></div>}
             {(id===2 || id===3 || id===4 || id===5 || id===6) && <div className="dice__point5__black"></div>}
             {id===6  && <div className="dice__point6__black"></div>}
             {(id===4 || id===5 || id===6) && <div className="dice__point7__black"></div>}
        </div>
    )}

export default DiceWhite1;