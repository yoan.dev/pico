import React, {Component} from 'react';

import {getCompoScrolling} from '../../../constants/scrollingSection';
import './SectionScrolling.css';

class SectionScrolling extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentSection: 'solo',
        }
    }
    goodSectionScrolling = (prevSection) => {
        const currentSectionCopy = this.state.currentSection;
        setTimeout(() => {
            this.setState(() => {
                return {
                    currentSection: prevSection,
                    prevSection: currentSectionCopy,
                };
            })
        },500);
        
    }
    render() {
        const {currentSection} = this.state;
        const {component} = this.props;
        let className;
        const components = getCompoScrolling(component, currentSection);
        const isSameSection = components.className === 'section__scrolling' ? true : false;
        if (this.state.currentSection !== component) {
            this.goodSectionScrolling(component);
        }
        if (components.height !==-1) {
            className = components.className+'--'+components.height;
        } else {
            className = components.className;
        }
        return(
            <div className="section__scrolling__notMove">
                {components[3]}
                <div className={className}>
                    {components[1]}
                    {!isSameSection && components[2]}
                </div>
            </div>
        );
    }
}

export default SectionScrolling;


