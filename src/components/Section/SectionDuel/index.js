import React from 'react';

import HeaderHome from '../../Header/HeaderHome';
import SectionDuelContent from './SectionDuelContent';
import './SectionDuel.css';

const SectionDuel = () =>
    <div className="section__duel">
        <HeaderHome/>
        <SectionDuelContent />
    </div>

export default SectionDuel;