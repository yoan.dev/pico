import React from 'react';
import BannerDuel from '../../Tools/BannerDuel';
import './SectionDuelContent.css';

const SectionDuelContent = () =>
    <div className='section__duel__content'>
        {[0,1].map(id => <BannerDuel id={id} />)}
    </div>

export default SectionDuelContent;