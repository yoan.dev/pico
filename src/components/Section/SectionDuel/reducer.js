import {NAV_DUEL_CHANGE} from '../../../constants/actionsTypes';

const INITIAL_STATE = {
    duelNav: [0,0,0],
};

const duelReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case NAV_DUEL_CHANGE: {
            return applyChangeDuelNav(state, action);
        }
        default: return state;
    }
}
const applyChangeDuelNav = (state, action) => {
    const duelNav = [...state.duelNav];
    const prevFocus = duelNav.indexOf(1);
    duelNav[prevFocus] = 0;
    duelNav[action.id] = 1;
    return {...state, duelNav};
}

export default duelReducer;