import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import {doChangeHome} from '../../../Home/actions';
import {doChangeSoloContent} from './actions';
import {getIdFocus, getMode} from './selectors';

import * as ROUTES from '../../../../constants/routes';
import arrow from '../../../../img/arrow.png';
import {dataBlocLevel} from '../../../../constants/data/dataBlocLevel';
import {scrollByWheelSolo} from '../../../../constants/functions';
import SectionSoloNav from '../SectionSoloNav';
import BlocLevel from '../../Tools/BlocLevel';
import BannerSolo from '../../Tools/BannerSolo';
import './SectionSoloContent.css';

class SectionSoloContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modeChange: '',
        }
    }
    changeMode = props => {
        const {modeChange} = this.state;
        if (modeChange !== props) {
            this.setState({modeChange: props});
            return true;
        }
        return false;
    }
    render() {
        const {mode, onChangeSoloContent, idFocus} = this.props;
        const isArrowLeft = (mode === 'medium' || mode === 'difficult');
        const isArrowRight = (mode === 'easy' || mode === 'medium');
        const arrowLeftFocus = mode === 'medium'
            ? 0 : 1;
        const arrowRightFocus = mode === 'easy'
            ? 1 : 2;
        return(
            
            <div className="section__solo__content">
                {   
                    idFocus === -1
                    ?<SectionSoloNav />
                    :<div>
                        <div className="section__solo__content__header">
                            {isArrowLeft && 
                                <img 
                                    src={arrow} 
                                    alt={'arrowLeft'} 
                                    className="section__solo__content__arrowLeft" 
                                    onClick={() => onChangeSoloContent(idFocus, arrowLeftFocus)}
                                />}
                            <BannerSolo 
                                mode={mode} 
                                className="banner__solo--small"
                            />
                            {isArrowRight && 
                                <img 
                                    src={arrow} 
                                    alt={'arrowRight'} 
                                    className="section__solo__content__arrowRight" 
                                    onClick={() => onChangeSoloContent(idFocus, arrowRightFocus)}
                                />}
                        </div>
                        <SectionSoloContentBlocLevels changeMode={this.changeMode} mode={mode}/>
                    </div>
                }   
            </div>
        );
    }
}

class SectionSoloContentBlocLevels extends Component {
    constructor(props) {
        super(props);
        this.elementScrollRef = element => {
            this.elementScroll = element;
        }
    }
    render() {
        const {mode, changeMode} = this.props;
        const isModeChange = changeMode(mode);
        const getTop0 = isModeChange ? '0px' : '';
        return(
            <div className="section__solo__content__blocLevels">
        <div 
        style={{top: getTop0}}
        className="section__solo__content__blocLevels__scroll"
        ref={this.elementScrollRef}
        onWheel={event => scrollByWheelSolo(event, this.elementScroll)}
        >
            {dataBlocLevel[mode].map((focus, index) => 
                <Link className="bloc__level__link" to={ROUTES.PLAY+ROUTES.SOLO} key={index} >
                    <BlocLevel key={index} focus={focus} mode={mode}>
                        {index}
                    </BlocLevel>
                </Link>
            )}
        </div>
    </div>
        );
    }
} 
const mapStateToProps = state => ({
    idFocus: getIdFocus(state),
    mode: getMode(state, getIdFocus(state)),
});

const mapDispatchToProps = dispatch => ({
    onChangeSoloContent: (id, arrow) => dispatch(doChangeSoloContent(id, arrow)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SectionSoloContent);