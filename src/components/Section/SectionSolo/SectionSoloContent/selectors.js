const getIdFocus = state => state.soloState.soloNav.indexOf(1);
const getMode = (state, id) => state.soloState.soloNavName[id];
export {
    getIdFocus,
    getMode,
};
