import {CONTENT_SOLO_CHANGE} from '../../../../constants/actionsTypes';

const doChangeSoloContent = (id, arrow) => ({
    type: CONTENT_SOLO_CHANGE,
    id,
    arrow,
});

export {
    doChangeSoloContent,
};