import React from 'react';

import BannerSolo from '../../Tools/BannerSolo';
import './SectionSoloNav.css';

const SectionSoloNav = () => 
    <div className="section__solo__nav">
        <BannerSolo mode="Easy" id={0}/>
        <BannerSolo mode="Medium" id={1} />
        <BannerSolo mode="Difficult" id={2} />
    </div>

export default SectionSoloNav;