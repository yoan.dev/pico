import React from 'react';

import HeaderHome from '../../Header/HeaderHome';
import SectionSoloContent from './SectionSoloContent';
import './SectionSolo.css';

const SectionSolo = () => 
    <div className="section__solo">
        <HeaderHome />
        <SectionSoloContent/>
    </div>

export default SectionSolo;

