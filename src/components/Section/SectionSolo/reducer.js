import {
    NAV_SOLO_CHANGE,
    CONTENT_SOLO_CHANGE,
    } from '../../../constants/actionsTypes';

const INITIAL_STATE = {
    soloNav: [0,0,0],
    soloNavName: ['easy', 'medium', 'difficult'],
    isBannerSoloBig: true,
};

const soloReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case NAV_SOLO_CHANGE: {
            return applyChangeSoloNav(state, action);
        }
        case CONTENT_SOLO_CHANGE: {
            return applyChangeSoloContent(state, action);
        }
        default: return state;
    }
}

const applyChangeSoloNav = (state, action) => {
    
    if (action.className === 'banner__solo--small') {
        const isBannerSoloBig = true;
        const soloNav = [...state.soloNav];
        const prevIdFocus = soloNav.indexOf(1);
        soloNav[prevIdFocus] = 0;
        return {...state, soloNav, isBannerSoloBig};
    } else if (action.className === 'banner__solo') {
        const isBannerSoloBig = false;
        const soloNav = [...state.soloNav];
        soloNav[action.id] = 1;
        return {...state, soloNav, isBannerSoloBig};
    }
}
const applyChangeSoloContent = (state, action) => {
    const soloNav = [...state.soloNav];
    if (action.id === 0) {
        soloNav[action.id] = 0;
        soloNav[1] = 1;
    } else if (action.id === 1) {
        soloNav[action.id] = 0;
        if (action.arrow === 0) {
            soloNav[0] = 1;
        } else if (action.arrow === 2) {
            soloNav[2] = 1;
        }
    } if (action.id === 2) {
        soloNav[action.id] = 0;
        soloNav[1] = 1;
    }
    return {...state, soloNav};
}

export default soloReducer;