import React from 'react';

import './BlocLevel.css';

const BlocLevel = ({focus, children, mode}) => 

    <div className={data[focus].class} >
        <div className={data[focus].classTxt} >{children+1}</div>
        {(data[focus].class !== 'bloc__level__incompleted') && <div className="bloc__level__focus__bubble"></div>}
    </div>

export default BlocLevel;

const data = {
    0: {
        class: 'bloc__level__incompleted',
        classTxt: 'bloc__level__incompleted__text',
    },
    1: {
        class: 'bloc__level__completed',
        classTxt: 'bloc__level__completed__text',
    },
    2: {
        class: 'bloc__level__focus',
        classTxt: 'bloc__level__focus__text',
    },
};