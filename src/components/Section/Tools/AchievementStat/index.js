import React from 'react';

import './AchievementStat.css';

const AchievementStat = ({data}) => 
    <div className="achievement__stat">
        {data.map(data => 
            <div className="achievement__stat__text">{data}</div>
        )}
    </div>

export default AchievementStat;
