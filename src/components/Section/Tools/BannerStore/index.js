import React from 'react';

import {dataDiceDessign} from '../../../../constants/data/dataDiceDessign';
import points from '../../../../img/achievementPoints.png';
import './BannerStore.css';

const BannerStore = ({nbItems}) => 
    <div className={classBannerStore[nbItems].class}>
        <div className="banner__store__container__ribbon">
            <div className={classBannerStore[nbItems].classTxt+'--1'}></div>
            <div className={classBannerStore[nbItems].classTxt+'--3'}></div>
            <div className={classBannerStore[nbItems].classTxt}>
                {classBannerStore[nbItems].sentence}
            </div>
            <div className={classBannerStore[nbItems].classTxt+'--2'}></div>
            
            <div className={classBannerStore[nbItems].classTxt+'--4'}></div>
        </div>
        {goodArr(nbItems).map(id => 
            <BannerStoreItem key={id} id={id} nbItems={nbItems} /> 
        )}
    </div>


const BannerStoreItem = ({id, nbItems}) => 
    <div className="banner__store__item">
        <div className="banner__store__container1">
            <div className="banner__store__container2">
                {[1,2,3,4,5].map(value => {
                    return withProps(dataDiceDessign[nbItems][id], value);
                }
                )}
            </div>
            <div className="banner__store__container__buy">
                <div className="banner__store__buy__text">
                    {classBannerStore[nbItems].arrCost[id]}
                </div>
                <img src={points} alt='points' className="banner__store__buy__img" />
            </div>
        </div>
        
    </div>
    
export default BannerStore;

const goodArr = nbItems => {
    let arr = [];
    for (let i = 0; i < nbItems; i++) {
        arr.push(i);
    }
    return arr;
};
const classBannerStore = {
    8: {
        class: 'banner__store--786',
        classTxt:'banner__store__text--786',
        sentence: 'Dice for beginners',
        arrCost: [75,75,75,75,100,100,125,125],
    },
    4: {
        class: 'banner__store--434',
        classTxt: 'banner__store__text--434',
        sentence: 'Dice for expert',
        arrCost: [200,200,250,250],
    },
    2: {
        class: 'banner__store--258',
        classTxt: 'banner__store__text--258',
        sentence: 'To handle with care !',
        arrCost: [500,500],
    },
};

const withProps = (Component, value) => <Component id={value} />;
