import React from 'react';

import AchievementPoints from '../../../Header/Tools/AchievementPoints';
import './AchievementIncompleted.css';

const AchievementIncompleted = ({data, updated}) => {
    
    return(
        <div className="achievement__incompleted">
            <div className="achievement__incompleted__container1">
                <div className="achievement__incompleted__container2">
                    <div className="achievement__incompleted__body">
                        <div className="achievement__incompleted__body--bubble">
                            <div className="achievement__incompleted__title">
                                {data.text1}{data.number.length !==1 && updated.toDo }{data.text2}
                            </div>
                        </div>
                        <div className="achievement__incompleted__body--bubble2"></div>
                        {data.number[0] && <AchievementBarCount data={data} updated={updated} />}
                        <AchievementPoints style="small" points={data.points} />
                    </div>
                </div>
            </div>
        </div>
    );}

const AchievementBarCount = ({updated}) => {
    const width = ((updated.count / updated.toDo) * 240) + 'px';
    console.log(parseInt(width))
    return(
    <div className="achievement__bar__count__container">
        <div className="achievement__bar__count__content" style={{width: width}}></div>
        <div className="achievement__bar__count__text">{updated.count+'/'+updated.toDo}</div>
        {parseInt(width)>17 && <div className="achievement__bar__count__animHide" style={{width: width}}>
            <div className="achievement__bar__count__anim"></div>
        </div>}
    </div>
    )}


export default AchievementIncompleted;

//count max 240px
