import { NAV_SOLO_CHANGE } from '../../../../constants/actionsTypes';

const doChangeSoloNav = (id, className) => ({
    type: NAV_SOLO_CHANGE,
    id,
    className,
});

export {
    doChangeSoloNav,
};