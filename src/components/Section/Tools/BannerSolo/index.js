import React from 'react';
import {connect} from 'react-redux';
import {doChangeSoloNav} from './actions';

import emptyStar from '../../../../img/emptyStar.png';
import star from '../../../../img/star.png';
import bannerSolo from '../../../../img/bannerSolo.png';
import './BannerSolo.css';

const BannerSolo = ({mode, onNavFocus, id, className = "banner__solo"}) => {
    const marginLeft = mode === 'easy' ? '42px' : '';
    const marginRight = mode === 'difficult' ? '42px' : '';
    return(
        <div 
            style={{marginLeft: marginLeft, marginRight: marginRight}}
            className={className} 
            onClick={() => onNavFocus(id, className)}
        >
            <div className="banner__solo__container1">
                <div className="banner__solo__container2">
                    <div className="banner__solo__body">
                        <div className="banner__solo__text">
                            {mode}
                        </div>
                        <div className="banner__solo__container__star">
                            <img src={emptyStar} alt="emptyStar" className="banner__solo__starImg" />
                            <img src={emptyStar} alt="emptyStar" className="banner__solo__starImg" />
                            <img src={emptyStar} alt="emptyStar" className="banner__solo__starImg" />
                        </div>
                    </div>
                    <img src={bannerSolo} alt="bannerSolo" className="banner__solo__bannerImg" />
                </div>
            </div>
        </div>
    );}

const mapDispatchToProps = dispatch => ({
    onNavFocus: (id, className) => dispatch(doChangeSoloNav(id, className)),
})

export default connect(null, mapDispatchToProps)(BannerSolo);
