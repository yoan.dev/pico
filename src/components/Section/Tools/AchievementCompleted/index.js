import React from 'react';

import './AchievementCompleted.css';

const AchievementCompleted = ({data}) => {
    return(
        <div className="achievement__completed">
            <div className="achievement__completed__container1">
                <div className="achievement__completed__container2">
                    <div className="achievement__completed__body">
                        <div className="achievement__completed__body--bubble">
                            <div className="achievement__completed__title">
                                {data.completed}
                            </div>
                        </div>
                        <div className="achievement__completed__body--bubble2"></div>
                        <div className="achievement__completed__title__text">
                            Completed
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AchievementCompleted;