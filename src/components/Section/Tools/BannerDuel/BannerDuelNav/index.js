import React from 'react';

import ButtonDuelNav from '../../../../Button/DuelNav';
import './BannerDuelNav.css';

const BannerDuelNav = () =>
    <div className="banner__duel__nav">
        {[0,1,2].map(id => <ButtonDuelNav key={id} id={id} />)}
    </div>

export default BannerDuelNav;