import React from 'react';

import BannerDuelNav from './BannerDuelNav';
import BannerDuelContent from './BannerDuelContent';
import './BannerDuel.css';

const BannerDuel = ({id}) => 
    <div className="banner__duel">
        {id===1 && <BannerDuelNav />}
        <BannerDuelContent id={id} />
    </div>

export default BannerDuel;