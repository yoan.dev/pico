import React from 'react';

import './BannerDuelContent.css';

const BannerDuelContent = ({id}) =>
    <div className="banner__duel__content">
        <div className="banner__duel__content__container1">
            <div className="banner__duel__content__container2">
                <div className="banner__duel__content__body">
                    <div className="banner__duel__content__text">
                        {id ? 'Only one will survive !' : 'Duel an opponent'}
                    </div>
                </div>
                <div className="banner__duel__content__go" style={{opacity: '0'}}>Go!</div>
            </div>
        </div>
    </div>

export default BannerDuelContent;