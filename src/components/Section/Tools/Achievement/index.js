import React from 'react';

import AchievementIncompleted from '../AchievementIncompleted';
import AchievementCompleted from '../AchievementCompleted';
import './Achievement.css';

const Achievement = ({data, achievements}) => {
    const updated = goodNumberAchievement(achievements[data.id]);
    return(
        <div className="achievement">
            {updated.lastToDo
                ? <AchievementCompleted data={data} updated={updated} /> 
                : <AchievementIncompleted data={data} updated={updated} />
            }
        </div>
    );}

export default Achievement;

const goodNumberAchievement = (achievement) => {
    let lastId = achievement.length-1;
    const count = achievement[lastId];
    const lastToDo = achievement[lastId-1];
    while (count >= achievement[0]) {
        achievement = achievement.filter(a => a > count);
    }
    if (achievement[0] === undefined) {
        return {
            count,
            lastToDo,
        };
    }
    return {
        count,
        toDo: achievement[0],
    };
};
