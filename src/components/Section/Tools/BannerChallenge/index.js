import React from 'react';

import BannerChallengeNav from './BannerChallengeNav';
import BannerChallengeContent from './BannerChallengeContent';
import './BannerChallenge.css';

const BannerChallenge = ({id}) => 
    <div className="banner__challenge">
        <BannerChallengeNav id={id} />
        <BannerChallengeContent id={id} />
    </div>

export default BannerChallenge;
