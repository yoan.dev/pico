import React from 'react';

import timer from '../../../../../img/timer.png';
import chain from '../../../../../img/chain.png';
import puzzle from '../../../../../img/puzzle.png';
import record from '../../../../../img/record.png';
import './BannerChallengeContent.css';

const BannerChallengeContent = ({id}) =>
    <div className="banner__challenge__content">
        <div className="banner__challenge__content__container1">
            <div className="banner__challenge__content__container2">
                <div className="banner__challenge__content__body">
                    <div className={data[id].classTxt}>
                        {data[id].txt}
                    </div>
                </div>
                <img src={data[id].img} alt={data[id].alt} className="banner__challenge__content__img" />  
                {id !== 6 && <BannerChallengeContentRecord id={id} />}
                <div className={data[id].classGo} style={{opacity: '0'}}>Go!</div>
            </div>
        </div>
    </div>

const BannerChallengeContentRecord = ({id}) =>
<div className="banner__challenge__content__record">
    <img src={record} alt={"record"} className="banner__challenge__content__record__img" />
    <div className="banner__challenge__content__record__text">{data[id].record}</div>
</div>

const data = {
    0: {
        txt: 'Try to do the bigger chain !',
        record: 'record: ',
        img: chain,
        alt: 'chain',
        classTxt: 'banner__challenge__content__text',
        classGo: 'banner__challenge__content__go',
    },
    3: {
        txt: 'Do your best with the timer !',
        record: 'record: ',
        img: timer,
        alt: 'timer',
        classTxt: 'banner__challenge__content__text',
        classGo: 'banner__challenge__content__go',
    },
    6: {
        txt: 'Can you solve these puzzles ?',
        img: puzzle,
        alt: 'puzzle',
        classTxt: 'banner__challenge__content__text--mid',
        classGo: 'banner__challenge__content__go--mid',
    },
};

export default BannerChallengeContent;