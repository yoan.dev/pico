import React from 'react';

import ButtonChallengeNav from '../../../../Button/ChallengeNav';
import './BannerChallengeNav.css';

const BannerChallengeNav = ({id}) => 
    <div className="banner__challenge__nav">
        {[0+id,1+id,2+id].map(id => <ButtonChallengeNav key={id} id={id} /> )}
    </div>

export default BannerChallengeNav;