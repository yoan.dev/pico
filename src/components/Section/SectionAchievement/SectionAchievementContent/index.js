import React,{Component} from 'react';
import {connect} from 'react-redux';
import {getId, getAchievements} from './selectors';

import {scrollByWheelAchievement} from '../../../../constants/functions';
import {isFocus} from '../../../../constants/data/dataAchievementScrolling';
import Achievement from '../../Tools/Achievement';
import AchievementStat from '../../Tools/AchievementStat';
import './SectionAchievementContent.css';

class SectionAchievementContent extends Component {
    constructor(props) {
        super(props);
        this.elementScroll = null;
        this.elementScrollRef = element => {
            this.elementScroll = element;
        }
        this.state = {
            changeFocus: 0,
        };
    }
    changeMode = props => {
        const {changeFocus} = this.state;
        if (changeFocus !== props) {
            this.setState({changeFocus: props});
            return true;
        }
        return false;
    }
    render() {
        const {id, achievements} = this.props;
        const data=isFocus[id].data
        const height=isFocus[id].height;
        const isChangeMode = this.changeMode(id);
        const getTop = isChangeMode ? '0px' : '';
        console.log(achievements)
        return(
            <div className="section__achievement__content">
                <div
                    style={{height: height, top:getTop}} 
                    ref={this.elementScrollRef} 
                    className="section__achievement__contentScroll"
                    onWheel={event => scrollByWheelAchievement(event, this.elementScroll, height)}
                >
                    { id !== 4
                    ? data.map(data => <Achievement 
                                            key={data.id} 
                                            data={data} 
                                            achievements={achievements}
                    /> )
                    :  <AchievementStat data={data}/>}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    id: getId(state),
    achievements: getAchievements(state, getId(state)),
});

export default connect(mapStateToProps)(SectionAchievementContent);