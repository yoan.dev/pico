const getId = state => state.achievementState.achievementNav.indexOf(1);
const getAchievements = (state, id) => state.achievementState[id];

export {
    getId,
    getAchievements,
};