import React from 'react';

import HeaderAchievement from '../../Header/HeaderAchievement';
import SectionAchievementNav from './SectionAchievementNav';
import SectionAchievementContent from './SectionAchievementContent';
import './SectionAchievement.css';

const SectionAchievement = () => {
    return(
        <div className="section__achievement">
            <HeaderAchievement />
            <div className="section__achievement__body">
                <SectionAchievementNav/>
                <SectionAchievementContent />
            </div>
        </div>
    );
}

export default SectionAchievement;
