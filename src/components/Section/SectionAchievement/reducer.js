import {NAV_ACHIEVEMENT_CHANGE} from '../../../constants/actionsTypes';

const INITIAL_STATE = {
    achievementNav: [1,0,0,0,0],
    0: {
        0: [1,5,20,50,100,0],
        1: [1,5,20,50,100,0],
        2: [1,5,20,50,100,0],
        3: [3,0],
        4: [3,0],
        5: [3,0],
        6: [3,0],
    },
    1: {
        0: [2,10,25,50,100,0],
        1: [2,10,25,50,100,0],
        2: [2,10,25,50,100,0],
        3: [1,5,10,20,30,0],
        4: [1,5,10,20,30,0],
        5: [1,5,10,20,30,0],
        6: [1,5,10,25,50,0],
        7: [1,5,10,25,50,0],
        8: [1,5,10,25,50,0],
    },
    2: {
        0: [300,600,1000,1500,2000,2300,2600,3000,680],
        1: [1,5,10,25,50,100,250,500,1000,255],
        2: [1,5,10,25,50,100,250,58],
        3: [2,4,6,8,10,0],
        4: [1,5,10,25,50,100,0],
        5: [1,5,10,25,50,100,250,500,0],
        6: [1,5,10,25,50,100,250,500,0],
        7: [1,5,10,25,50,100,250,500,0],
        8: [1,5,10,25,50,100,0],
        9: [2,4,6,8,10,0],
        10: [2,4,6,0],
        11: [3,0],
    },
    3: {
        0: [2,5,10,20,30,40,50,60,22],
        1: [1,10,25,50,100,250,500,1000,274],
        2: [1,10,25,50,100,250,500,1000,104],
        3: [1,10,25,50,100,250,500,1000,2500,5000,1327],
        4: [10,9],
        5: [25,21],
        6: [100,78],
        7: [111,0],
        8: [111,0],
        9: [111,0],
        10: [2,5,10, 5,20,25,30,0],
        11: [2,5,10, 5,20,25,30,0,0],
        12: [2,5,10, 5,20,25,30,0,0],
        13: [1,5,10,0],
        14: [1,5,10,0],
    },
};

const achievementReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case NAV_ACHIEVEMENT_CHANGE: {
            return applyChangeAchievementNav(state, action);
        }
        default: return state;
    }
}

const applyChangeAchievementNav = (state, action) => {
    const achievementNav = [...state.achievementNav];
    const prevIndex = achievementNav.indexOf(2);
    const prevIdFocus = achievementNav.indexOf(1);
    if (prevIndex !== -1) achievementNav[prevIndex] = 0;
    [achievementNav[prevIdFocus], achievementNav[action.id]] = [2, 1];
    return {...state, achievementNav};
}

export default achievementReducer;