import React from 'react';

import {achievementNavNames} from '../../../../constants/data/dataNavConfig';
import ButtonAchievementNav from '../../../Button/AchievementNav';
import './SectionAchievementNav.css';

const SectionAchievementNav = () => 
      
    <div className="section__achievement__navbar">
        {[0,1,2,3,4].map(id => 
            <ButtonAchievementNav key={id} id={id}>
                <div className="section__achievement__navbar__button__title">{achievementNavNames[id]}</div>
            </ButtonAchievementNav>
        )}
    </div>


export default SectionAchievementNav;



