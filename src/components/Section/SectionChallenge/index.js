import React from 'react';

import HeaderHome from '../../Header/HeaderHome';
import SectionChallengeContent from './SectionChallengeContent';
import './SectionChallenge.css';

const SectionChallenge = () => 
    <div className="section__challenge">
        <HeaderHome />
        <SectionChallengeContent />
    </div>

export default SectionChallenge;