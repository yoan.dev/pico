import {NAV_CHALLENGE_CHANGE} from '../../../constants/actionsTypes';

const INITIAL_STATE = {
    challengeNav: [
        1,0,0,
        2,0,0,
        2,0,0
    ],
};

const challengeReducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case NAV_CHALLENGE_CHANGE: {
            return applyChangeChallengeNav(state, action);
        }
        default: return state;
    }
}
const applyChangeChallengeNav = (state, action) => {
    const challengeNav = [...state.challengeNav];
    const prevFocus = challengeNav.indexOf(1);
    challengeNav[prevFocus] = 2;
    challengeNav[action.id] = 1;
    return {...state, challengeNav};
}

export default challengeReducer;