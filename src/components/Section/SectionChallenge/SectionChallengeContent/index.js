import React from 'react';

import BannerChallenge from '../../Tools/BannerChallenge';
import './SectionChallengeContent.css';

const SectionChallengeContent = () => 

    <div className="section__challenge__content">
        {[0,3,6].map(id => <BannerChallenge key={id} id={id} />)}
    </div>

export default SectionChallengeContent;