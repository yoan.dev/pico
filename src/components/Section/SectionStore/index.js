import React, {Component} from 'react';

import {scrollByWheelStore} from '../../../constants/functions';
import HeaderAchievement from '../../Header/HeaderAchievement';
import BannerStore from '../Tools/BannerStore';
import './SectionStore.css';

const SectionStore = () =>
    <div className="section__store">
        <HeaderAchievement />
        <SectionStoreContent />
    </div>

class SectionStoreContent extends Component {
    constructor(props) {
        super(props);
        this.elementScrollRef = element => {
            this.elementScroll = element;
        }
    }
    render() {
        return(
        <div className="section__store__container">
            <div 
                style={{height: '1550px'}}
                ref={this.elementScrollRef} 
                className="section__store__scroll"
                onWheel={event => scrollByWheelStore(event, this.elementScroll)}
            >
                {[8,4,2].map(nbItems =>
                <BannerStore key={nbItems} nbItems={nbItems} />)}
            </div>
        </div>
        );
    }
}

export {
SectionStoreContent,
};
export default SectionStore;