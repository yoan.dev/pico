import {createStore, applyMiddleware} from 'redux';
import {createLogger} from 'redux-logger';
import rootReducer from './combineReducers';

const logger = createLogger();

const store = createStore(
    rootReducer,
    undefined,
    applyMiddleware(logger)
);

export default store;