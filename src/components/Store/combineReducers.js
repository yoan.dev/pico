import {combineReducers} from 'redux';
import homeReducer from '../Home/reducer';
import soloReducer from '../Section/SectionSolo/reducer';
import challengeReducer from '../Section/SectionChallenge/reducer';
import duelReducer from '../Section/SectionDuel/reducer';
import achievementReducer from '../Section/SectionAchievement/reducer';
import storeReducer from '../Section/SectionStore/reducer';


const rootReducer = combineReducers({
    homeState: homeReducer,
    soloState: soloReducer,
    challengeState: challengeReducer,
    duelState: duelReducer,
    achievementState: achievementReducer,
    storeState: storeReducer,
});

export default rootReducer;