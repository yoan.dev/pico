import React from 'react';
import { Link } from 'react-router-dom';

import {homeNav} from '../../constants/data/dataNavConfig';
import ButtonFooter from '../Button/Footer';
import './Navigation.css';

const Navigation = () => 

    <div className="navbar">
        {[0,1,2,3,4].map(id => 
            <Link className="navbar__link" to={homeNav[id].url} key={id} >
                <ButtonFooter src={homeNav[id].src} id={id}  >
                    <div className="navbar__button__title">{homeNav[id].name}</div>
                </ButtonFooter>
            </Link>
        )}
    </div>


export default Navigation;
