import React from 'react';
import PlayerLevel from '../Tools/PlayerLevel';
import AchievementPoints from '../Tools/AchievementPoints';
import './HeaderHomeTop.css';

const HeaderHomeTop = () => 
    <div className="header__home__top">
        <PlayerLevel/>
        <AchievementPoints allPoints={"55"} />
    </div>

export default HeaderHomeTop;