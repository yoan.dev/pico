import React from 'react';

import ButtonContainer from '../Tools/ButtonContainer';
import PlayerLogo from '../Tools/PlayerLogo';
import PlayerTitle from '../Tools/PlayerTitle';
import TrophyPoints from '../Tools/TrophyPoints';
import ChoiceDice from '../../Button/ChoiceDice';
import './HeaderHomeBottom.css';

const HeaderHomeBottom = () =>
    <div className="header__home__bottom__left">
        <PlayerLogo/>
        <div className="header__home__bottom__right">
            <PlayerTitle/>
            <div className="header__home__bottom__inline">
                <TrophyPoints/>
                <ChoiceDice />
                <ButtonContainer/>
            </div>
        </div>
    </div>

export default HeaderHomeBottom;