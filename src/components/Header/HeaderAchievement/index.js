import React from 'react';

import PlayerLevel from '../Tools/PlayerLevel';
import AchievementPoints from '../Tools/AchievementPoints';
import './HeaderAchievement.css';

const HeaderAchievement = () => 
    <div className="header__achievement">
        <PlayerLevel/>
        <AchievementPoints  style={"big"} allPoints={"55"} />
    </div>

export default HeaderAchievement;