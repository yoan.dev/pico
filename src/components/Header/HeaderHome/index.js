import React from 'react';

import HeaderHomeTop from '../HeaderHomeTop';
import HeaderHomeBottom from '../HeaderHomeBottom';
import './HeaderHome.css';

const HeaderHome  = () => (
    <div className="header__home">
        <HeaderHomeTop/>
        <HeaderHomeBottom/>
    </div>
);

export default HeaderHome;