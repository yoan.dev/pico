import React from 'react';
import './PlayerTitle.css';

const PlayerTitle = () =>
    <div className="player__title">
        <div className="player__title__text">Albert the beginner</div>
    </div>

export default PlayerTitle;