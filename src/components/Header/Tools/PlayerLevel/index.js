import React from 'react';

import './PlayerLevel.css';

const PlayerLevel = () => 
    <div className="player__level__container">
        <div className="player__level__content" style={{width: "30px"}}>
            
        </div>
        <div className="player__level__text">2/20</div>
    </div>

export default PlayerLevel;