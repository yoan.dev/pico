import React from 'react';

import trophy from '../../../../img/trophy.png';
import './TrophyPoints.css';

const TrophyPoints = () =>
    <div className="trophy__points">
        <div className="trophy__points__content">
            <img
                src={trophy}
                alt="trophy"
                className="trophy__points__img"
            />
            <div className="trophy__points__text"> 17</div>
        </div>
    </div>

export default TrophyPoints;