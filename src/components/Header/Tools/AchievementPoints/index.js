import React from 'react';

import achievementPoints from '../../../../img/achievementPoints.png';
import './AchievementPoints.css';

const AchievementPoints = ({style, points, allPoints}) => {
    
    const getClassName = style === 'small' ? "achievement__points--small":"achievement__points";
    const isAllPoints = style === 'small' ? points : allPoints;
    return(
    <div className={getClassName}>
        <img 
            src={achievementPoints}
            alt="achievementPoints"
            className="achievement__points__img" />
        <div className="achievement__points__text">{isAllPoints}</div>
    </div>
    );
}

export default AchievementPoints;