import React from 'react';

import ButtonOption from '../../../Button/Option';
import ButtonSocial from '../../../Button/Social';
import './ButtonContainer.css';

const ButtonContainer = () =>
    <div className="button__container">
        <ButtonSocial/>
        <ButtonOption/>
    </div>

export default ButtonContainer;