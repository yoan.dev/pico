const getValue = (state, id) => (state.achievementState.achievementNav)[id];

export {
    getValue,
};