import React from 'react';
import {connect} from 'react-redux';
import {doChangeAchievementNav} from './actions';
import {getValue} from './selectors';

import './ButtonAchievementNav.css';

const ButtonAchievementNav = ({value, id, onChangeFocus, children}) => 

    <div className={data[value]} onClick={() => onChangeFocus(id)} >
        {children}
    </div>

const mapStateToProps = (state, {id}) => ({
    value: getValue(state, id),
});
const mapDispatchToProps = dispatch => ({
    onChangeFocus: id => dispatch(doChangeAchievementNav(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ButtonAchievementNav);

const data = ['button__achievement__nav', 'button__achievement__nav--focus', 'button__achievement__nav--prevFocus'];