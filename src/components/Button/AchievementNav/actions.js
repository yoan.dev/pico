import { NAV_ACHIEVEMENT_CHANGE } from '../../../constants/actionsTypes';

const doChangeAchievementNav = id => ({
    type: NAV_ACHIEVEMENT_CHANGE,
        id,
});

export {
    doChangeAchievementNav,
};