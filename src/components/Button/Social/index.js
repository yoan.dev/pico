import React from 'react';

import social from '../../../img/social.png';
import './Social.css';

const ButtonSocial = () => 
    <div className="button__social">
        <img 
            className="button__social__img"
            src={social} 
            alt="social"
         />
    </div>

export default ButtonSocial;