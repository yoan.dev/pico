import React from 'react';
import {connect} from 'react-redux';
import {doChangeHomeNav} from './actions';
import {getValue} from './selectors';

import './Footer.css';

const ButtonFooter = ({value, id, src, onChangeFocus, children}) => 
    <div className={data[value].class} onClick={() => onChangeFocus(id)} >
        <img className={data[value].classImg} src={src} alt={src} />
        {value === 1 ? children : null}
    </div>


const mapStateToProps = (state, {id}) => ({
    value: getValue(state, id),
});
const mapDispatchToProps = dispatch => ({
    onChangeFocus: id => dispatch(doChangeHomeNav(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ButtonFooter);

const data = {
    0: {
        class: "button__footer",
        classImg: "button__img__footer",
    },
    1: {
        class: "button__footer--focus",
        classImg: 'button__img__footer--focus',
    },
    2: {
        class: "button__footer--prevFocus",
        classImg: "button__img__footer--prevFocus",
    },
    'default': {
        class: 'button__footer--default',
        classImg: "button__footer__img--defaut",
    },
};
