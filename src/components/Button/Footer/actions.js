import { NAV_HOME_CHANGE } from '../../../constants/actionsTypes';

const doChangeHomeNav = id => ({
    type: NAV_HOME_CHANGE,
        id,
});

export {
    doChangeHomeNav,
};