import React from 'react';

import './Option.css';

const ButtonOption = () => 
    <div className="button__option">
        <div className="button__option__line"></div>
        <div className="button__option__line"></div>
        <div className="button__option__line"></div>
    </div>

export default ButtonOption;