import React from 'react';

import choiceDice from '../../../img/choiceDice.png';
import './ChoiceDice.css';

const ChoiceDice = () =>
    <div className="choice__dice">
        <img src={choiceDice} alt='choiceDice' className="choice__dice__img" />
    </div>

export default ChoiceDice;