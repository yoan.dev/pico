import React from 'react';
import {connect} from 'react-redux';
import {doChangeChallengeNav} from './actions';
import {getValue} from './selectors';

import challengeMiniDice from '../../../img/challengeMiniDice.png';
import './ButtonChallengeNav.css';

const ButtonChallengeNav = ({id, value, onChangeFocus}) => {

    const onClickValue = value !== 0 ? (() => onChangeFocus(id)) : null;
    return(
        <div className={data[value].class} onClick={onClickValue}>
            <div className={data[value].classTxt}>{nbDices[id]}</div>
            <img src={challengeMiniDice} alt={"challengeMiniDice"} className={data[value].classImg} />
            {(value !== 0) && <div className="button__challenge__nav__focus__bubble"></div>}
        </div>
    );
}
const mapStateToProps = (state, {id}) => ({
    value: getValue(state, id),
});
const mapStateToDispatch = dispatch => ({
    onChangeFocus: id => dispatch(doChangeChallengeNav(id)),
});
export default connect(mapStateToProps, mapStateToDispatch)(ButtonChallengeNav);

const data = {
    0: {
        class: 'button__challenge__nav__block',
        classTxt: 'button__challenge__nav__text__block',
        classImg: 'button__challenge__nav__img__block',
    },
    1: {
        class: 'button__challenge__nav__focus',
        classTxt: 'button__challenge__nav__text__unblock',
        classImg: 'button__challenge__nav__img__unblock',
    },
    2: {
        class: 'button__challenge__nav__unblock',
        classTxt: 'button__challenge__nav__text__unblock',
        classImg: 'button__challenge__nav__img__unblock',
    },
};
const nbDices = [
    '3', '4', '5',
    '3', '4', '5',
    '4', '5', '6',
];