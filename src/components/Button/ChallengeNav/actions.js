import {NAV_CHALLENGE_CHANGE} from '../../../constants/actionsTypes';

const doChangeChallengeNav = id => ({
    type: NAV_CHALLENGE_CHANGE,
    id,
});

export {
    doChangeChallengeNav,
};