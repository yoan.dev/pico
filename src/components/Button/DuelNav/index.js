import React from 'react';
import {connect} from 'react-redux';
import {doChangeDuelNav} from './actions';
import {getValue} from './selectors';

import './ButtonDuelNav.css';

const ButtonDuelNav = ({id, value, onChangeFocus}) => {
     let className = 'button__duel__nav--notFocus';
    if (value === 1) className = 'button__duel__nav--focus';
    return(
        <div className={className} onClick={() => onChangeFocus(id)}>
            <div className='button__duel__nav__text' >{idContent[id]}</div>
            {(value !== 0) && <div className='button__duel__nav__focus__bubble'></div>}
        </div>
    );
}
const mapStateToProps = (state, {id}) => ({
    value: getValue(state, id),
});
const mapStateToDispatch = dispatch => ({
    onChangeFocus: id => dispatch(doChangeDuelNav(id)),
});

export default connect(mapStateToProps, mapStateToDispatch)(ButtonDuelNav);

const idContent = ['3 players', '5 players', '10 players'];
