import {NAV_DUEL_CHANGE} from '../../../constants/actionsTypes';

const doChangeDuelNav = id => ({
    type: NAV_DUEL_CHANGE,
    id,
});

export {
    doChangeDuelNav,
};