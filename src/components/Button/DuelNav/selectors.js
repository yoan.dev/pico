const getValue = (state, id) => state.duelState.duelNav[id];

export {
    getValue,
};