import React, {Component} from 'react';
import {withFirebase} from '../../login/Firebase';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';

import SignIn from '../../login/SignIn';
import SignUp from '../../login/SignUp';
import Home from '../Home';
import './App.css';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authUser: null,
        }
    }
    componentDidMount() {
        this.listener = this.props.firebase.auth.onAuthStateChanged(authUser => {
            authUser
                ? this.setState({authUser})
                : this.setState({authUser: null});
        });
    }
    componentWillUnmount() {
        this.listener();
    }
    render() {
        const {authUser} = this.state;
        return(
                <Router>
                    <div className="app">
                        <Switch>
                            <Route exact path={ROUTES.LANDING} component={SignIn} />
                            <Route exact path={ROUTES.SIGN_UP} component={SignUp} />

                            <Route exact path={ROUTES.HOME+ROUTES.SECTION_SOLO} component={Home} />
                            <Route exact path={ROUTES.HOME+ROUTES.SECTION_CHALLENGE} component={Home} />
                            <Route exact path={ROUTES.HOME+ROUTES.SECTION_DUEL} component={Home} />
                            <Route exact path={ROUTES.HOME+ROUTES.SECTION_ACHIEVEMENT} component={Home} />
                            <Route exact path={ROUTES.HOME+ROUTES.SECTION_STORE} component={Home} />

                            <Route exact path={ROUTES.PLAY+ROUTES.SOLO} component={Home} />

                        </Switch>
                    </div>
                </Router>
        );
    }
}

export default withFirebase(App);