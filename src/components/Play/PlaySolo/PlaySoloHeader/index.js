import React from 'react';

import HeaderHomeTop from '../../../Header/HeaderHomeTop';
import './PlaySoloHeader.css';

const PlaySoloHeader = () => 
    <div className="play__solo__header">
        <HeaderHomeTop />
    </div>

export default PlaySoloHeader;

// lvl + pt hf
// 'facile' + btn choix dès + social + option