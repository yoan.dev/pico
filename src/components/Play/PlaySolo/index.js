import React from 'react';

import PlaySoloHeader from './PlaySoloHeader';
import './PlaySolo.css';

const PlaySolo = () => 
    <div className="play__solo">
        <PlaySoloHeader />
    </div>

export default PlaySolo;