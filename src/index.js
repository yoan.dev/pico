import React from 'react';
import ReactDOM from 'react-dom';
import Firebase, {FirebaseContext} from './login/Firebase';
import {Provider} from 'react-redux';
import * as serviceWorker from './serviceWorker';

import App from './components/App';
import store from './components/Store';
import './index.css';

ReactDOM.render(
    <FirebaseContext.Provider value={new Firebase()} >
        <Provider store={store}>
            <App />
        </Provider>
    </FirebaseContext.Provider>,
    document.getElementById('root'));

serviceWorker.unregister();
