const estimElo = (p1, p2) => Math.round((1 / (1 + Math.pow(10, ((p1 - p2 ) / 400 )))) * 10000) / 10000;
const newElo = (elo, estimELo, k, res) => {
    let newElo = elo + k * (res - estimELo);
    if (newElo < 0) newElo = 0;
    return newElo;
};
const calcElo = (j1Elo, j2Elo, res, k) => {
    const estimJ1 = estimElo(j2Elo, j1Elo);
    const estimJ2 = estimElo(j1Elo, j2Elo);
    if (res === 0) {
        const newEloJ1 = newElo(j1Elo, estimJ1, k, res);
        const newEloJ2 = newElo(j2Elo, estimJ2, k, (1-res));
        const x = newEloJ1-j1Elo;
        const y = newEloJ2-j2Elo;
        console.log('--------------')
        console.log('J1 elo: '+j1Elo+'\nwin a: '+(estimJ1*100+'%')+'\nresultat: '+res+'\nJ1 new elo: '+newEloJ1+'\npoints: '+x);
        console.log('J2 elo: '+j2Elo+'\nwin a: '+(estimJ2*100+'%')+'\nresultat: '+(1-res)+'\nJ2 new elo: '+newEloJ2+'\npoints: '+y);
        return {
            1: newEloJ1,
            2: newEloJ2,
        };
    }
    if (res === 1) {
        const newEloJ1 = Math.round(newElo(j1Elo, estimJ1, k, res));
        const newEloJ2 = Math.round(newElo(j2Elo, estimJ2, k, (1-res)));
        const x = newEloJ1-j1Elo;
        const y = newEloJ2-j2Elo;
        console.log('--------------')
        console.log('J1 elo: '+j1Elo+'\nwin a: '+(estimJ1*100+'%')+'\nresultat: '+res+'\nJ1 new elo: '+newEloJ1+'\npoints: '+x);
        console.log('J2 elo: '+j2Elo+'\nwin a: '+(estimJ2*100+'%')+'\nresultat: '+(1-res)+'\nJ2 new elo: '+newEloJ2+'\npoints: '+y);
        return {
            1: newEloJ1,
            2: newEloJ2,
        };
    }
    if (res === 0.5) {
        const newEloJ1 = newElo(j1Elo, estimJ1, k, res);
        const newEloJ2 = newElo(j2Elo, estimJ2, k, res);
        const x = newEloJ1-j1Elo;
        const y = newEloJ2-j2Elo;
        console.log('J1 elo: '+j1Elo+'\nwin a: '+(estimJ1*100+'%')+'\nresultat: '+res+'\nJ1 new elo: '+newEloJ1+'\npoints: '+x);
        console.log('J2 elo: '+j2Elo+'\nwin a: '+(estimJ2*100+'%')+'\nresultat: '+res+'\nJ2 new elo: '+newEloJ2+'\npoints: '+y);
        return {
            1: newEloJ1,
            2: newEloJ2,
        };
    }
}