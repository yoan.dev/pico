import React from 'react';
import SectionSolo from '../components/Section/SectionSolo';
import SectionChallenge from '../components/Section/SectionChallenge';
import SectionDuel from '../components/Section/SectionDuel';
import SectionAchievement from '../components/Section/SectionAchievement';
import SectionStore from '../components/Section/SectionStore';

import HeaderHome from '../components/Header/HeaderHome';
import HeaderHomeTop from '../components/Header/HeaderHomeTop';
import HeaderHomeBottom from '../components/Header/HeaderHomeBottom';
import SectionAchievementNav from '../components/Section/SectionAchievement/SectionAchievementNav';

import SectionSoloContent from '../components/Section/SectionSolo/SectionSoloContent';
import SectionChallengeContent from '../components/Section/SectionChallenge/SectionChallengeContent';
import SectionDuelContent from '../components/Section/SectionDuel/SectionDuelContent';
import SectionAchievementContent from '../components/Section/SectionAchievement/SectionAchievementContent';
import {SectionStoreContent} from '../components/Section/SectionStore';


const getCompoScrolling = (currentCompo, nextCompo) => {
    let className, height=-1;
    let component1, component2, component3;
    let components = {};
    switch(currentCompo) {
        case 'solo':
            switch(nextCompo) {
                case 'solo':
                    component1 = <SectionSolo/>;
                    nextCompo = 0;
                break;
                case 'challenge':
                    component1 = <SectionSoloContent />;
                    component2 =  <SectionChallengeContent />;
                    component3 =  <HeaderHome />;
                    height = '521';
                    nextCompo = 1;
                break;
                case 'duel':
                    component1 = <SectionSoloContent />;
                    component2 =  <SectionDuelContent />;
                    component3 =  <HeaderHome />;
                    height = '521';
                    nextCompo = 2;
                break;
                case 'achievement':
                    component1 = <div>
                                    <HeaderHomeBottom />
                                    <SectionSoloContent />
                                </div>;
                    component2 = <div>
                                    <SectionAchievementNav />
                                    <SectionAchievementContent />
                                </div>;
                    component3 = <HeaderHomeTop />;
                    height = '605';
                    nextCompo = 3;
                break;
                case 'store':
                    component1 = <div>
                                    <HeaderHomeBottom />
                                    <SectionSoloContent />
                                </div>;
                    component2 =  <SectionStoreContent />;
                    component3 = <HeaderHomeTop />;
                    height = '605';
                    nextCompo = 4;
                break;
            }
            currentCompo = 0;
        break;
        case 'challenge':
            switch(nextCompo) {
                case 'solo':
                    component1 = <SectionChallengeContent/>;
                    component2 = <SectionSoloContent />;
                    component3 =  <HeaderHome />;
                    height = '521';
                    nextCompo = 0;
                break;
                case 'challenge':
                    component1 = <SectionChallenge />;
                    nextCompo = 1;
                break;
                case 'duel':
                    component1 = <SectionChallengeContent />;
                    component2 =  <SectionDuelContent />;
                    component3 =  <HeaderHome />;
                    height = '521';
                    nextCompo = 2;
                break;
                case 'achievement':
                    component1 = <div>
                                    <HeaderHomeBottom />
                                    <SectionChallengeContent />
                                </div>;
                    component2 = <div>
                                    <SectionAchievementNav />
                                    <SectionAchievementContent />
                                </div>;
                    component3 = <HeaderHomeTop />;
                    height = '605';
                    nextCompo = 3;
                break;
                case 'store':
                    component1 = <div>
                                    <HeaderHomeBottom />
                                    <SectionChallengeContent />
                                </div>;
                    component2 =  <SectionStoreContent />;
                    component3 = <HeaderHomeTop />;
                    height = '605';
                    nextCompo = 4;
                break;
            }
            currentCompo = 1;
        break;
        case 'duel':
            switch(nextCompo) {
                case 'solo':
                    component1 = <SectionDuelContent/>;
                    component2 = <SectionSoloContent />;
                    component3 =  <HeaderHome />;
                    height = '521';
                    nextCompo = 0;
                break;
                case 'challenge':
                    component1 = <SectionDuelContent />;
                    component2 = <SectionChallengeContent />;
                    component3 =  <HeaderHome />;
                    height = '521';
                    nextCompo = 1;
                break;
                case 'duel':
                    component1 = <SectionDuel />;
                    nextCompo = 2;
                 break;
                case 'achievement':
                    component1 = <div>
                                    <HeaderHomeBottom />
                                    <SectionDuelContent />
                                </div>;
                    component2 = <div>
                                    <SectionAchievementNav />
                                    <SectionAchievementContent />
                                </div>;
                    component3 = <HeaderHomeTop />;
                    height = '605';
                    nextCompo = 3;
                break;
                case 'store':
                    component1 = <div>
                                    <HeaderHomeBottom />
                                    <SectionDuelContent />
                                </div>;
                    component2 =  <SectionStoreContent />;
                    component3 = <HeaderHomeTop />;
                    height = '605';
                    nextCompo = 4;
                break;
            }
            currentCompo = 2;
        break;
        case 'achievement':
            switch(nextCompo) {
                case 'solo':
                    component1 = <div>
                                    <SectionAchievementNav />
                                    <SectionAchievementContent />
                                </div>;
                    component2 = <div>
                                    <HeaderHomeBottom />
                                    <SectionSoloContent />
                                </div>;
                    component3 = <HeaderHomeTop />;
                    height = '605';
                    nextCompo = 0;
                break;
                case 'challenge':
                    component1 = <div>
                                    <SectionAchievementNav />
                                    <SectionAchievementContent />
                                </div>;
                    component2 = <div>
                                    <HeaderHomeBottom />
                                    <SectionChallengeContent />
                                </div>;
                    component3 = <HeaderHomeTop />;
                    height = '605';
                    nextCompo = 1;
                break;
                case 'duel':
                    component1 = <div>
                                    <SectionAchievementNav />
                                    <SectionAchievementContent />
                                </div>;
                     component2 = <div>
                                    <HeaderHomeBottom />
                                    <SectionDuelContent />
                                </div>; 
                    component3 = <HeaderHomeTop />; 
                     height = '605';          
                    nextCompo = 2;
                    break;
                case 'achievement':
                    component1 = <SectionAchievement />;
                    nextCompo = 3;
                break;
                case 'store':
                    component1 = <div>
                                    <SectionAchievementNav />
                                    <SectionAchievementContent />
                                </div>;
                    component2 =  <SectionStoreContent />;
                    component3 = <HeaderHomeTop />; 
                    height = '605';  
                    nextCompo = 4;
                break;
            }
            currentCompo = 3;
        break;
        case 'store':
            switch(nextCompo) {
                case 'solo':
                    component1 = <SectionStoreContent/>;
                    component2 = <div>
                                    <HeaderHomeBottom />
                                    <SectionSoloContent />
                                </div>;
                    component3 = <HeaderHomeTop />; 
                    height = '605'; 
                    nextCompo = 0;
                break;
                case 'challenge':
                    component1 = <SectionStoreContent/>;
                    component2 = <div>
                                    <HeaderHomeBottom />
                                    <SectionChallengeContent />
                                </div>;
                    component3 = <HeaderHomeTop />; 
                    height = '605'; 
                    nextCompo = 1;
                break;
                case 'duel':
                    component1 = <SectionStoreContent/>;
                    component2 = <div>
                                    <HeaderHomeBottom />
                                    <SectionDuelContent />
                                </div>; 
                    component3 = <HeaderHomeTop />; 
                    height = '605'; 
                    nextCompo = 2;
                    break;
                case 'achievement':
                    component1 = <SectionStoreContent/>;
                    component2 = <div>
                                    <SectionAchievementNav />
                                    <SectionAchievementContent />
                                </div>;
                    component3 = <HeaderHomeTop />; 
                    height = '605'; 
                    nextCompo = 3;
                break;
                case 'store':
                    component1 = <SectionStore />;
                    nextCompo = 4;
                break;
            }
            currentCompo = 4;
        break;
    }
    
        if (currentCompo-nextCompo > 0) {
            className = "section__scrolling__right";
        } else if(currentCompo-nextCompo < 0) {
            className = "section__scrolling__left";
        } else if (currentCompo === nextCompo) {
            className = "section__scrolling";
        }
    if ( className === 'section__scrolling') {
        components = {
            1: component1,
            className,
        };
    }
    if (className === 'section__scrolling__right') {
         components = {
            1: component2,
            2: component1,
            3: component3,
            className,
            height,
        };
    } else if (className === 'section__scrolling__left') {
         components = {
            1: component1,
            2: component2,
            3: component3,
            className,
            height,
        };
    }
    return components;
}

export {
    
    getCompoScrolling,
};
