import {dataAchievement} from './dataAchievement';

export const isFocus = {
    '0': {
        data:dataAchievement.solo,
        height: '958px',
    },
    '1': {
        data:dataAchievement.challenge,
        height: '1326px',
    },
    '2': {
        data:dataAchievement.duel,
        height: '1628px',
    },
    '3': {
        data:dataAchievement.general,
        height: '2030px',
    },
    '4': {
        data: dataAchievement.stat,
    },
};