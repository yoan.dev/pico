import * as ROUTES from '../routes';
import achievement from '../../img/achievement.png';
import challenge from '../../img/challenge.png';
import solo from '../../img/solo.png';
import duel from '../../img/duel.png';
import store from '../../img/store.png';

export const dataNavConfigBannerChallenge = {
    0: [1,0,0],
    1: [2,0,0],
    2: [2,0,0],
};

export const dataNavChallengeNb = {
    0: ['3','4','5'],
    1: ['3','4','5'],
    2: ['4','5','6'],
};

export const achievementNavNames = {
    0: 'solo',
    1: 'challenge',
    2: 'duel',
    3: 'general',
    4: 'stat',
};
export const homeNav = {
    0: {
        url: ROUTES.HOME+ROUTES.SECTION_SOLO,
        src: solo,
        name: 'solo',
    },
    1: {
        url: ROUTES.HOME+ROUTES.SECTION_CHALLENGE,
        src: challenge,
        name: 'challenge',
    },
    2: {
        url: ROUTES.HOME+ROUTES.SECTION_DUEL,
        src: duel,
        name: 'duel',
    },
    3: {
        url: ROUTES.HOME+ROUTES.SECTION_ACHIEVEMENT,
        src: achievement,
        name: 'achievement',
    },
    4: {
        url: ROUTES.HOME+ROUTES.SECTION_STORE,
        src: store,
        name: 'store',
    },
};