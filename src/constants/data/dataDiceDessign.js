import React from 'react';

import DiceBlack1 from '../../components/Dice/DiceBlack1';
import DiceBlue1 from '../../components/Dice/DiceBlue1';
import DiceBrown1 from '../../components/Dice/DiceBrown1';
import DiceGreen1 from '../../components/Dice/DiceGreen1';
import DicePink1 from '../../components/Dice/DicePink1';
import DiceRed1 from '../../components/Dice/DiceRed1';
import DiceWhite1 from '../../components/Dice/DiceWhite1';
import DiceYellow1 from '../../components/Dice/DiceYellow1';

const dataDiceDessign = {
    8: {
        0: DiceBlack1,
        1: DiceBlue1,
        2: DiceBrown1 ,
        3: DiceGreen1,
        4: DicePink1,
        5: DiceRed1,
        6: DiceWhite1,
        7: DiceYellow1, 
    },
    4: {
        0: DiceBlack1,
        1: DiceBlue1,
        2: DiceBrown1,
        3: DiceGreen1,
    },
    2: {
        0:DiceBlack1,
        1: DiceBlue1,
    },
};


export {
    dataDiceDessign,
};