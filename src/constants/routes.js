export const LANDING = '/';
export const SIGN_UP = '/signUp';

export const HOME = '/home';
//WHEN WE ARE IN HOME .../home/...
export const SECTION_SOLO = '/sectionSolo';
export const SECTION_CHALLENGE = '/sectionChallenge';
export const SECTION_DUEL = '/sectionDuel';
export const SECTION_ACHIEVEMENT = '/sectionAchievement';
export const SECTION_STORE = '/sectionStore';

//WHEN WE ARE IN SECTION_ACHIEVEMENT .../home/sectionAchievement/...
export const SOLO = '/solo';
export const CHALLENGE = '/challenge';
export const DUEL = '/duel';
export const GENERAL = '/general';
export const STAT = '/stat';

//WHEN WE ARE IN SECTION_SOLO
export const EASY = '/easy';
export const MEDIUM = '/medium';
export const DIFFICULT = '/difficult';

//WHEN WE ARE IN PLAY

export const PLAY = '/play';

