import * as ROUTES from './routes';

const getGoodFocusHomeNav = url => {
    let array = [];
    if (url.includes(ROUTES.SECTION_SOLO)) {
        return array = [1,0,0,0,0];
    } else if (url.includes(ROUTES.SECTION_CHALLENGE)) {
        return array = [0,1,0,0,0];
    } else if (url.includes(ROUTES.SECTION_DUEL)) {
        return array = [0,0,1,0,0];
    } else if (url.includes(ROUTES.SECTION_ACHIEVEMENT)) {
        return array = [0,0,0,1,0];
    } else if (url.includes(ROUTES.SECTION_STORE)) {
        return array = [0,0,0,0,1];
    } else return array = ["default",0,0,0,0];
}
const getGoodFocusAchievementNav = url => {
    let array = [];
    const achivUrl = ROUTES.SECTION_ACHIEVEMENT;
    if (url.includes(achivUrl+ROUTES.SOLO)) {
        return array = [1,0,0,0,0];
    } else if (url.includes(achivUrl+ROUTES.CHALLENGE)) {
        return array = [0,1,0,0,0];
    } else if (url.includes(achivUrl+ROUTES.DUEL)) {
        return array = [0,0,1,0,0];
    } else if (url.includes(achivUrl+ROUTES.GENERAL)) {
        return array = [0,0,0,1,0];
    } else if (url.includes(achivUrl+ROUTES.STAT)) {
        return array = [0,0,0,0,1];
    } else return array = ["default",0,0,0,0];
}
const changeFocus = (array, position, prevPosition) => {
    const indexDefault = array.indexOf("default");
    let getFocus = array.map(item => item = 0);
    if (indexDefault !== -1) {
        getFocus[indexDefault] = 2;
    } else {
        getFocus[prevPosition] = 2;
    }
    getFocus[position] = 1;
    if (getFocus.includes("default")) {
        const getIndex = getFocus.indexOf("default");
        getFocus[getIndex] = 2;
    }
    return getFocus;
    
}

const scrollByWheelAchievement = (event, element, height) => {
    let topMax;
    const top = parseInt(element.style.top);
    const y = event.deltaY;

    switch(height) {
        case '958px': 
            topMax = -390;
        break;
        case '1326px': 
            topMax = -760;
        break;
        case '1628px': 
            topMax = -1080;
        break;
        case '2030px': 
            topMax = -1460;
        break;
        default: console.log('error scrolling in achievement.');
    }
    if (element.style.top === '') {
        element.style.top = '0px';
    }
    if (y < 0 && top < 0) {
        element.style.top = top + 40 +'px';
    } else if (y > 0 && top > topMax) {
        element.style.top = top - 40 +'px';
    }
}

const scrollByWheelSolo = (event, element) => {
    const top = parseInt(element.style.top);
    const y = event.deltaY;

    if (element.style.top === '') {
        element.style.top = '0px';
    }
    if (y < 0 && top < 0) {
        element.style.top = top + 40 +'px';
    } else if (y > 0 && top > -680) {
        element.style.top = top - 40 +'px';
    }
}
const scrollByWheelStore = (event, element) => {
    const top = parseInt(element.style.top);
    const y = event.deltaY;

    if (element.style.top === '') {
        element.style.top = '0px';
    }
    if (y < 0 && top < 0) {
        element.style.top = top + 40 + 'px';
    } else if (y > 0 && top > -950) {
        element.style.top = top - 40 + 'px';
    }
}

export {
    getGoodFocusHomeNav,
    getGoodFocusAchievementNav,
    changeFocus,
    scrollByWheelAchievement,
    scrollByWheelSolo,
    scrollByWheelStore,
};
